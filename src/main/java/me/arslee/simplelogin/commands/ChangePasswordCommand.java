package me.arslee.simplelogin.commands;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.arslee.simplelogin.AuthManager;
import me.arslee.simplelogin.SessionManager;
import me.arslee.simplelogin.config.Config;
import me.arslee.simplelogin.config.Messages;

public class ChangePasswordCommand implements CommandExecutor {
    AuthManager authManager;
    SessionManager sessionManager;
    Config config;

    public ChangePasswordCommand(AuthManager authManager, SessionManager sessionManager, Config config) {
        this.authManager = authManager;
        this.sessionManager = sessionManager;
        this.config = config;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Messages messages = config.messages;

        if (!(sender instanceof Player)) {
            sender.sendMessage(messages.playersOnly);
            return true;
        }

        if (args.length != 2) {
            return false;
        }

        String password = args[0].trim();
        String confirmation = args[1].trim();

        if (!password.equals(confirmation)) {
            sender.sendMessage(messages.mismatchedPasswords);
            return true;
        }

        try {
            if (!authManager.isRegistered(sender.getName())) {
                sender.sendMessage(messages.notRegistered);
            } else if (!sessionManager.isLoggedIn((Player) sender)) {
                sender.sendMessage(messages.loginFirst);
            } else {
                authManager.changePassword(sender.getName(), password);
                sender.sendMessage(messages.successfulPasswordChange);
            }
        } catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            sender.sendMessage(messages.internalError);
        }

        return true;
    }
}
