package me.arslee.simplelogin;

import java.net.InetAddress;
import java.util.HashMap;

import org.bukkit.entity.Player;

public class SessionManager {
    private HashMap<String, Session> loggedIn;
    private int sessionTimeout;

    public SessionManager(int sessionTimeout) {
        this.loggedIn = new HashMap<String, Session>();
        this.sessionTimeout = sessionTimeout;
    }

    public void beginSession(Player player) {
        loggedIn.put(player.getName(), new Session(player));
    }

    public boolean isLoggedIn(Player player) {
        Session session = loggedIn.get(player.getName());

        if (session != null) {
            InetAddress sessionAddress = session.getAddress().getAddress();
            InetAddress playerAddress = player.getAddress().getAddress();
            if (!sessionAddress.equals(playerAddress)) {
                return false;
            }

            if (session.getLastSeen() != null && session.getLastSeenDuration() > sessionTimeout) {
                loggedIn.remove(player.getName());
            }
        }

        return loggedIn.containsKey(player.getName());
    }

    public void endSession(Player player) {
        loggedIn.get(player.getName()).setLastSeen();
    }
}
