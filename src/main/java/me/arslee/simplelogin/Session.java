package me.arslee.simplelogin;

import java.net.InetSocketAddress;
import java.time.Duration;
import java.time.LocalDateTime;

import org.bukkit.entity.Player;

public class Session {
    private String name;
    private LocalDateTime lastSeen;
    private InetSocketAddress address;

    public Session(String name, LocalDateTime loginDateTime, InetSocketAddress address) {
        this.name = name;
        this.lastSeen = loginDateTime;
        this.address = address;
    }

    public Session(Player player) {
        this.name = player.getName();
        this.lastSeen = null;
        this.address = player.getAddress();
    }

    public void setLastSeen() {
        lastSeen = LocalDateTime.now();
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getLastSeen() {
        return lastSeen;
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public long getLastSeenDuration() {
        return Duration.between(lastSeen, LocalDateTime.now()).abs().getSeconds();
    }
}
