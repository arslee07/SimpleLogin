package me.arslee.simplelogin;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthManager {
    Connection connection;

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom rnd = new SecureRandom();

    public AuthManager(Connection connection) throws SQLException {
        this.connection = connection;
        connection.createStatement().executeUpdate(
                "create table if not exists credential (id integer primary key, name string unique, salt string, password string)");
    }

    private static String generateSalt() {
        StringBuilder sb = new StringBuilder(16);
        for (int i = 0; i < 16; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    private static String hashPassword(String password, String salt) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] hash = md.digest(password.concat(salt).getBytes());

        StringBuilder hexString = new StringBuilder();
        for (byte b : hash) {
            hexString.append(String.format("%02x", b));
        }

        return hexString.toString();
    }

    public void register(String name, String password) throws SQLException, NoSuchAlgorithmException {
        String salt = generateSalt();
        String hash = hashPassword(password, salt);

        PreparedStatement statement = connection
                .prepareStatement("insert into credential (name, salt, password) values (?, ?, ?)");
        statement.setString(1, name);
        statement.setString(2, salt);
        statement.setString(3, hash);
        statement.executeUpdate();
    }

    public boolean verifyLogin(String name, String password) throws SQLException, NoSuchAlgorithmException {
        PreparedStatement statement = connection
                .prepareStatement("select salt, password from credential where name = ?");
        statement.setString(1, name);
        ResultSet rs = statement.executeQuery();

        String salt = rs.getString(1);
        String hash = rs.getString(2);

        return hashPassword(password, salt).equals(hash);
    }

    public void changePassword(String name, String password) throws SQLException, NoSuchAlgorithmException {
        String salt = generateSalt();
        String hash = hashPassword(password, salt);

        PreparedStatement statement = connection
                .prepareStatement("update credential set salt = ?, password = ? where name = ?");
        statement.setString(1, salt);
        statement.setString(2, hash);
        statement.setString(3, name);
        statement.executeUpdate();
    }

    public boolean isRegistered(String name) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("select * from credential where name = ?");
        statement.setString(1, name);
        ResultSet rs = statement.executeQuery();

        return rs.next();
    }
}
