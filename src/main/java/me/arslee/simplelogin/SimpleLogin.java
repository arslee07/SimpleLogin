package me.arslee.simplelogin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.config.Configuration;

import me.arslee.simplelogin.commands.ChangePasswordCommand;
import me.arslee.simplelogin.commands.LoginCommand;
import me.arslee.simplelogin.commands.RegisterCommand;
import me.arslee.simplelogin.config.Config;

public class SimpleLogin extends JavaPlugin implements Listener {
    private Connection connection;

    private Config config;
    private AuthManager authManager;
    private SessionManager sessionManager;

    @Override
    public void onDisable() {
        Bukkit.getLogger().info("ChatFormat disabled");
    }

    @Override
    public void onEnable() {
        Configuration bukkitConfiguration = getConfiguration();
        config = new Config(bukkitConfiguration);
        bukkitConfiguration.save();

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:plugins/SimpleLogin/simplelogin.db");
            authManager = new AuthManager(connection);
            sessionManager = new SessionManager(config.sessionTimeout);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        Server server = getServer();

        server.getPluginManager().registerEvents(this, this);
        server.getPluginManager().registerEvents(new InvulnerabilityListener(sessionManager), this);

        server.getPluginCommand("login").setExecutor(new LoginCommand(authManager, sessionManager, config));
        server.getPluginCommand("register").setExecutor(new RegisterCommand(authManager, sessionManager, config));
        server.getPluginCommand("changepassword")
                .setExecutor(new ChangePasswordCommand(authManager, sessionManager, config));

        Bukkit.getLogger().info("ChatFormat enabled");
    }

    @EventHandler
    void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        try {
            if (sessionManager.isLoggedIn(player)) {
                player.sendMessage(config.messages.autoLogin);
                sessionManager.beginSession(player);
            } else {
                boolean isRegistered = authManager.isRegistered(player.getName());
                player.sendMessage(
                        isRegistered ? config.messages.loginPrompt : config.messages.registerPrompt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    void onPlayerQuit(PlayerQuitEvent event) {
        sessionManager.endSession(event.getPlayer());
    }
}
