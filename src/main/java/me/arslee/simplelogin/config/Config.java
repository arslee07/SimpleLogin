package me.arslee.simplelogin.config;

import org.bukkit.util.config.Configuration;

public class Config {
    public final int sessionTimeout;
    public final Messages messages;

    public Config(Configuration bukkitConfiguration) {
        this.sessionTimeout = bukkitConfiguration.getInt("session-timeout", 300);
        this.messages = new Messages(bukkitConfiguration);
    }
}
