package me.arslee.simplelogin.config;

import org.bukkit.ChatColor;
import org.bukkit.util.config.Configuration;

public class Messages {
    public final String loginPrompt;
    public final String registerPrompt;
    public final String successfulLogin;
    public final String successfulRegister;
    public final String successfulPasswordChange;
    public final String autoLogin;
    public final String wrongPassword;
    public final String loginFirst;
    public final String notRegistered;
    public final String alreadyRegistered;
    public final String mismatchedPasswords;
    public final String playersOnly;
    public final String internalError;

    Messages(Configuration config) {
        this.loginPrompt = config.getString("messages.login-prompt",
                ChatColor.AQUA + "Please log in to your account: /login [password]");
        this.registerPrompt = config.getString("messages.register-prompt",
                ChatColor.AQUA + "Please register your account: /register [password] [password]");
        this.successfulLogin = config.getString("messages.successful-login",
                ChatColor.GREEN + "Successfully logged in!");
        this.successfulRegister = config.getString("messages.successful-register",
                ChatColor.GREEN + "Successfully registered!");
        this.successfulPasswordChange = config.getString("messages.successful-password-change",
                ChatColor.GREEN + "Password changed successfully!");
        this.autoLogin = config.getString("messages.auto-login", ChatColor.GREEN + "Automatically logged in!");
        this.wrongPassword = config.getString("messages.wrong-password", ChatColor.RED + "Wrong password.");
        this.notRegistered = config.getString("messages.not-registered",
                ChatColor.RED + "This account is not registered.");
        this.alreadyRegistered = config.getString("messages.already-registered",
                ChatColor.RED + "This account is already registered.");
        this.mismatchedPasswords = config.getString("messages.mismatched-passwords",
                ChatColor.RED + "Passwords do not match.");
        this.playersOnly = config.getString("messages.already-registered",
                ChatColor.RED + "This command is only available to players.");
        this.internalError = config.getString("messages.internal-error",
                ChatColor.RED + "An internal error occured!");
        this.loginFirst = config.getString("messages.login-first",
                ChatColor.RED + "Log in to accout in order to execute this command.");
    }
}
